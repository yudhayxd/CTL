﻿using UnityEngine;
using System.Collections;
using System;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    protected static T instance;
    //Returns the instance of this singleton.
    private void Awake()
    {
        if (instance != null && instance != this.gameObject.GetComponent<T>())
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this.gameObject.GetComponent<T>();
        }
        DontDestroyOnLoad(this.gameObject);
    }
    public static T getInstance()
    {
        if (instance == null)
        {
            instance = (T)FindObjectOfType(typeof(T));

            if (instance == null)
            {
                //Debug.LogError("An instance of " + typeof(T) + 
                //	" is needed in the scene, but there is none.");
                GameObject go = new GameObject("_" + typeof(T));
                DontDestroyOnLoad(go);
                instance = go.AddComponent<T>();
            }
        }

        return instance;
    }
}