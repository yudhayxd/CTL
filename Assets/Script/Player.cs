﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] PlayerSprite[] playerSprite;

    public int DASH_MULTIPLIER = 3;

    [HideInInspector] public int idCharacter;

    [HideInInspector] public string nameCharacter;
    [HideInInspector] public float healthPoint;
    [HideInInspector] public float energyPoint;
    [HideInInspector] public float damage;
    [HideInInspector] public float walkSpeed;
    [HideInInspector] public float energyDashDecreaseFrequency = 0.5f;
    [HideInInspector] public float energyIncreaseFrequency = 1;

    [HideInInspector] public bool isDashing;

    DatabaseCharacter db = DatabaseCharacter.databasePlayer;
    private float normalWalkingSpeed;
    public VirtualJoystick joystick;

    Animator anim;

    private void Start()
    {
        joystick = FindObjectOfType<VirtualJoystick>();
        anim = GetComponent<Animator>();

        normalWalkingSpeed = walkSpeed;
        //GetComponent<TrailRenderer>().enabled = false;
        InitCharacter();

    }

    private void InitCharacter()
    {
        switch (GameplayDataManager.getInstance().currentLevel)
        {
            case 1: InitDataCharacter(0); idCharacter = 0; break;
            case 2: InitDataCharacter(0); idCharacter = 0; break;
            case 3: InitDataCharacter(1); idCharacter = 1; break;
            case 4: InitDataCharacter(1); idCharacter = 1; break;
            case 5: InitDataCharacter(2); idCharacter = 2; break;
            case 6: InitDataCharacter(2); idCharacter = 2; break;
            case 7: InitDataCharacter(3); idCharacter = 3; break;
            case 8: InitDataCharacter(3); idCharacter = 3; break;
            case 9: InitDataCharacter(4); idCharacter = 4; break;
            case 10: InitDataCharacter(4); idCharacter = 4; break;
        }
        Debug.Log("Character data" + "ID: " + idCharacter + ", Name: " + nameCharacter + ", HP: " + healthPoint + ", EP: " + energyPoint + ", Damage: " + damage + ", Speed: " + walkSpeed);
    }

    private void InitDataCharacter(int _idPlayer)
    {
        Debug.Log("Data loaded");
        idCharacter = _idPlayer;
        nameCharacter = db.getName(_idPlayer);
        healthPoint = db.getHealth(_idPlayer); ;
        energyPoint = db.getEnergy(_idPlayer); ;
        damage = db.getDamage(_idPlayer); ;
        walkSpeed = db.getSpeed(_idPlayer); ;
    }

    private void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        if (healthPoint > 0)
        {
            transform.Translate(joystick.InputDirection.x * Time.deltaTime * walkSpeed, joystick.InputDirection.y * Time.deltaTime * walkSpeed, 0);
            //rigid.AddForce(joystick.InputDirection * speed * Time.deltaTime);
        }

        //Quaternion wanted_rotation = Quaternion.LookRotation()
        //transform.rotation = Quaternion.RotateTowards(transform.rotation, wanted_rotation, MaxTurnSpeed * Time.deltaTime);

        if (Input.GetButton("Fire1"))
        {

        }
        if (isDashing)
        {
            walkSpeed -= walkSpeed / 4;
            energyPoint -= energyDashDecreaseFrequency;
            damage += energyDashDecreaseFrequency;
            if (energyPoint <= 0)
            {
                ResetUndashValue();
            }
        }
        if (walkSpeed <= normalWalkingSpeed)
        {
            if (energyPoint > db.getHealth(idCharacter))
            {
                energyPoint = db.getHealth(idCharacter);
            }
            else
            {
                energyPoint += energyIncreaseFrequency;
            }
            ResetUndashValue();
            //GetComponent<TrailRenderer>().enabled = false;
        }
        UIManager.getInstance().UpdateHealthBar(healthPoint);

        // Dash Pressed, isDash Variable set true
        if(UIManager.getInstance().isDash){
            UIManager.getInstance().UpdateEnergyBar(energyPoint);

            if(energyPoint < 4){
                DASH_MULTIPLIER = 1;
                UIManager.getInstance().isDash = false;
            } 
            Dash();
        } else{
            DASH_MULTIPLIER = 3;
            
        }

    }

    private void ResetUndashValue()
    {
        isDashing = false;
        damage = 0;
        walkSpeed = normalWalkingSpeed;
        anim.SetBool("dash", false);
    }

    public void Dash()
    {
        // Debug.Log("Dash!!");
        walkSpeed = normalWalkingSpeed * DASH_MULTIPLIER;
        isDashing = true;
        anim.SetBool("dash", true);
        //GetComponent<TrailRenderer>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Enemy>())
        {
            Enemy enemy = collision.GetComponent<Enemy>();
            enemy.currentHealth -= damage;
            if (!isDashing)
            {
                anim.SetBool("hit", true);
                Invoke("delaySetHit", 1);
            }
        }
    }

    private void delaySetHit()
    {
        anim.SetBool("hit", false);
    }
}
