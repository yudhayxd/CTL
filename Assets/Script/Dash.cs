﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI; // Required when Using UI elements.

public class Dash : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{

    public Image energyBar;
    public float waitTime = 30.0f;
    bool dash = false;

    public void OnPointerDown(PointerEventData pointerEventData)
    {
        dash = true;
        //Output the name of the GameObject that is being clicked
        Debug.Log(name + "Game Object Click in Progress");
    }

    //Detect if clicks are no longer registering
    public void OnPointerUp(PointerEventData pointerEventData)
    {
        dash = false;
        Debug.Log(name + "No longer being clicked");
        // energyBar.enabled = false;
    }

    void Update(){
        
    }
}