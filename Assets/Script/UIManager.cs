﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{

    public Image healthBar;
    public Image energyBar;

    public Button dashButton;
    public bool isDash = false;

    private void Start()
    {
        // dashButton.onClick.AddListener(onClickDashButton);
        

        EventTrigger trigger = dashButton.GetComponent<EventTrigger>();

        EventTrigger.Entry pointerdown = new EventTrigger.Entry();
        pointerdown.eventID = EventTriggerType.PointerDown;
        pointerdown.callback.AddListener((data) => { OnPointerDownDelegate((PointerEventData)data); });
        trigger.triggers.Add(pointerdown);

        EventTrigger.Entry pointerup = new EventTrigger.Entry();
        pointerup.eventID = EventTriggerType.PointerUp;
        pointerup.callback.AddListener((data) => { OnPointerUpDelegate((PointerEventData)data); });
        trigger.triggers.Add(pointerup);
    }

    public void OnPointerDownDelegate(PointerEventData data)
    {
        isDash = true;
    }

    public void OnPointerUpDelegate(PointerEventData data)
    {
        isDash = false;
    }

    // private void onClickDashButton()
    // {
    //     if (FindObjectOfType<Player>() != null)
    //     {
    //         if (FindObjectOfType<Player>().healthPoint > 0)
    //         {
    //             FindObjectOfType<Player>().Dash();
    //         }

    //     }
    // }

    DatabaseCharacter db = DatabaseCharacter.databasePlayer;

    public void UpdateHealthBar(float healthPoint)
    {

        healthBar.transform.localScale = new Vector3(4 * (healthPoint / db.getHealth(FindObjectOfType<Player>().idCharacter)), 1, 0);
    }

    public void UpdateEnergyBar(float energyPoint)
    {
        FindObjectOfType<Player>().idCharacter = 1;
        energyBar.transform.localScale = new Vector3(4 * (energyPoint / db.getEnergy(FindObjectOfType<Player>().idCharacter)), 1, 0);
    }

    public void AttachGameOver()
    {

    }

}
