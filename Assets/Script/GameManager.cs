﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{

    public Player player;
    public Enemy[] enemy;

    private void Start()
    {
        GameplayDataManager.getInstance().currentLevel = 1;
        Debug.Log("Current level " + GameplayDataManager.getInstance().currentLevel);
        GeneratePlayer();
        for (int i = 0; i < enemy.Length; i++)
        {
            Instantiate(enemy[i], new Vector3(i, i, 0), Quaternion.identity);
        }
    }

    private void GeneratePlayer()
    {
        Instantiate(player, new Vector3(-7, 0, 0), Quaternion.identity);
    }

}
