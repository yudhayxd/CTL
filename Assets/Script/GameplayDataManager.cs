﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayDataManager : Singleton<GameplayDataManager>
{

    public int reachedLevel;
    public int currentLevel;
    public float[] scoreEachLevel;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
