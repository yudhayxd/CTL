﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatabaseCharacter  {

	public static DatabaseCharacter databasePlayer = new DatabaseCharacter();

	public DatabaseCharacter(){
		loadDatabase();
	}

	private List<ArrayList> playerData = new List<ArrayList>();
	private List<ArrayList> enemyData = new List<ArrayList>();

    private void loadDatabase()
    {								   //"Speed","Health","Energy", "Damage","Name"
        playerData.Add(new ArrayList(){   1f,      123f,    100f,		8f,       "Neutrophil"   });
		playerData.Add(new ArrayList(){   1f,      100f,    100f,		8f,       "Eosinophyl"   });
		playerData.Add(new ArrayList(){   1f,      100f,    100f,		8f,       "Basophyl"   });
		playerData.Add(new ArrayList(){   1f,      100f,    100f,		8f,       "Lymphocyte"   });
		playerData.Add(new ArrayList(){   1f,      100f,    100f,		8f,       "Monocyte"   });

										//"Speed","Health", "Damage"
		enemyData.Add(new ArrayList(){   10f,      500f,		8f});
		enemyData.Add(new ArrayList(){   10f,      500f,		8f});
		enemyData.Add(new ArrayList(){   10f,      500f,		8f});
		enemyData.Add(new ArrayList(){   10f,      500f,		8f});
		enemyData.Add(new ArrayList(){   10f,      500f,		8f});
    }

	public float getSpeed(int _idPlayer){
		return (float)playerData[_idPlayer][0];
	}
	public float getHealth(int _idPlayer){
		return (float)playerData[_idPlayer][1];
	}
	public float getEnergy(int _idPlayer){
		return (float)playerData[_idPlayer][2];
	}

	public float getDamage(int _idPlayer){
		return (float)playerData[_idPlayer][2];
	}

	public string getName(int _idPlayer){
		return (string)playerData[_idPlayer][4];
	}
}
